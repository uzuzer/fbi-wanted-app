export const environment = {
  production: false,
  usersUrl: 'http://localhost:4200/assets/users.json',
  fbiWantedUrl: 'https://api.fbi.gov/wanted/v1/list',
  fbiWantedEditedUrl: 'https://6190cfd541928b001768fe5f.mockapi.io/people'
};
