import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { AuthorizationService } from '../../services/authorization.service';

@Component({
  selector: 'fw-authorization',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})
export class AuthorizationComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<AuthorizationComponent>, 
    private authorizationService: AuthorizationService, private router: Router) { }

  public ngOnInit(): void { }

  public onSignIn(form: NgForm): void {
    const { login, password } = form.value;
    this.authorizationService.signIn(login, password).subscribe(user => {
      if (user) {
        this.router.navigate(['/home']);
        localStorage.setItem('field_offices', '');
        this.dialogRef.close();
      }
    });
    form.resetForm();
  }
}
