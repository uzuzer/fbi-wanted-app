import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';

import { AuthorizationComponent } from './components/authorization/authorization.component';

@NgModule({
  declarations: [
    AuthorizationComponent
  ],
  imports: [
    MatDialogModule,
    FormsModule,
    CommonModule
  ],
  exports: [
    AuthorizationComponent
  ]
})
export class AuthorizationModule { }
