import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ApiService } from '../../../shared/services/api/api.service';
import { User } from '../../../shared/interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  public currentUser = new BehaviorSubject<User>({} as User);
  
  constructor(private apiService: ApiService) { }

  public signIn(login: string, password: string): Observable<User> {
    return this.apiService.getUser(login, password).pipe(
      tap(user => this.currentUser.next(user)),
      tap(user => {
        if(user) {
          localStorage.setItem('login', user.login),
          localStorage.setItem('password', user.password)
        }
      })
    );
  }

  public signOut(): void {
    this.currentUser.next({} as User);
    localStorage.clear();
  }
}
