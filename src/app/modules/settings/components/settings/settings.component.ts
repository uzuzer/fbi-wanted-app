import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { FbiWantedService } from '../../../fbi-wanted/services/fbi-wanted.service';

@Component({
  selector: 'fw-settings',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  public selectedOption!: string;

  constructor(public fbiWantedService: FbiWantedService) { }

  public ngOnInit(): void { }

  public onSetFieldOffice(form: any) {
    const { inputName } = form.value;
    inputName.value ? localStorage.setItem('field_offices', inputName.value) : localStorage.setItem('field_offices', this.selectedOption);
    form.reset();
  }
}
