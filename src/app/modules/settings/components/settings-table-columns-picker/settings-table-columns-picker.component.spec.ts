import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsTableColumnsPickerComponent } from './settings-table-columns-picker.component';

describe('SettingsTableColumnsPickerComponent', () => {
  let component: SettingsTableColumnsPickerComponent;
  let fixture: ComponentFixture<SettingsTableColumnsPickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsTableColumnsPickerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsTableColumnsPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
