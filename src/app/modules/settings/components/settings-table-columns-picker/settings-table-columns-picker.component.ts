import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormArray } from '@angular/forms';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

import { TableColumnNames } from '../../../fbi-wanted/data/table-column-names';
import { FbiWantedService } from '../../../fbi-wanted/services/fbi-wanted.service';

@Component({
  selector: 'fw-settings-table-columns-picker',
  templateUrl: './settings-table-columns-picker.component.html',
  styleUrls: ['./settings-table-columns-picker.component.scss']
})
export class SettingsTableColumnsPickerComponent implements OnInit {
  public deleteIcon = faTrash;

  private columnName: number = 1;

  public tableColumnPickerForm = this.formBuilder.group({
    tableColumnPickers: this.formBuilder.array([])
  });

  constructor(private formBuilder: FormBuilder, public fbiWantedService: FbiWantedService) { }

  public ngOnInit(): void { 
    this.tableColumnPickers.push(this.formBuilder.group({ [this.columnName]: ['', Validators.required] }));
  }

  public getTableColumnNames(): (string | TableColumnNames)[] {
    return Object.values(TableColumnNames).filter(elem =>
      typeof elem === "string"
    );
  }

  public get tableColumnPickers(): FormArray {
    return this.tableColumnPickerForm.get('tableColumnPickers') as FormArray;
  }

  public addSelect(): void {
    if(this.columnName < 5) {
      this.columnName++;
      this.tableColumnPickers.push(this.formBuilder.group({ [this.columnName]: ['', Validators.required] }));
    }
  }

  public deleteSelect(index: number): void {
    this.tableColumnPickers.removeAt(index);
  }

  public onEditTable() {
    this.fbiWantedService.tableColumnNamesEdited = [];
    for(let i = 0; i < this.columnName; i++) {
      let columnName = Object.values(this.tableColumnPickers.get(i.toString())?.value)[0];
      if(columnName) {
        this.fbiWantedService.tableColumnNamesEdited.push(columnName as string);
      }
    }
    this.tableColumnPickers.reset();
  }
}
