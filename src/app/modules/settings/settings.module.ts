import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { MatSelectModule } from '@angular/material/select';

import { SettingsComponent } from './components/settings/settings.component';
import { SettingsTableColumnsPickerComponent } from './components/settings-table-columns-picker/settings-table-columns-picker.component';

@NgModule({
  declarations: [
    SettingsComponent,
    SettingsTableColumnsPickerComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    MatSelectModule,
    CommonModule
  ]
})
export class SettingsModule { }
