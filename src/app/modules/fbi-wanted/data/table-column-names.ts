export enum TableColumnNames {
  actions,
  name, 
  sex,
  race,
  fieldOffices,
}