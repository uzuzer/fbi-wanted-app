export enum InputDataTypes {
  number,
  string, 
  boolean,
  date
}