import { FormControl, AbstractControl, ValidatorFn } from '@angular/forms';

export class FormValidator {
  public selectedInputDataType!: string;

  public newInputsValidator = (control: FormControl): void | null => {
    if(control.value && control.value.search(/\d/) != -1 && this.selectedInputDataType === 'string') {
      control.setValue('');
    } else if(control.value && !Number(control.value) && this.selectedInputDataType === 'number') {
      control.setValue('');
    } else if(control.value && this.selectedInputDataType === 'boolean') {
      if(control.value.match(/\b0\b/)) {
        control.setValue('false');
      } else if(control.value.match(/\b1\b/)) {
        control.setValue('true');
      } else if(!control.value.match(/\b0\b|\b1\b|\bfalse\b|\btrue\b/)) {
        control.setValue('');
      }
    } else if(control.value && this.selectedInputDataType === 'date') {
      let date = new Date(control.value);
      if(date.toString() === 'Invalid Date') {
        control.setValue('');
      }
    }
    return null;
  }

  uniqueInputsNamesValidator(): ValidatorFn {
    return (controls: AbstractControl): { [key: string]: boolean } | null => {
      let valid = true;
      const uniqueControls = [...new Set(controls.value.map((control: FormControl) => Object.keys(control)[0]))];
      if(uniqueControls.length < controls.value.length) {
        valid = false;
      }
      return valid ? null : { 'duplicate': true };
    };
  }
}