import { Pipe, PipeTransform } from '@angular/core';
  
@Pipe({
    name: 'format'
})
export class FbiWantedPipe implements PipeTransform {
  public transform(str: string | number | undefined): string | number {
    return str || '---';
  }
}