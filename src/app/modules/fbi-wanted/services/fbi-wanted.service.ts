import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';
import { BehaviorSubject, Observable, forkJoin } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { map } from 'rxjs/operators';

import { Person } from '../../../shared/interfaces/person';
import { TableColumnNames } from '../data/table-column-names';
import { ApiService } from '../../../shared/services/api/api.service';
import { FbiWantedEditFormComponent } from '../components/fbi-wanted-edit-form/fbi-wanted-edit-form.component';

@UntilDestroy()
@Injectable({
  providedIn: 'root'
})
export class FbiWantedService {
  public persons!: Person;

  public requestFilters: any;
  public requestWithFilters: boolean = false;
  
  public minPage: number = 1;
  public totalNumberOfPersons: number = 0;
  public numberOfPersonsOnPage: number = 0;

  public tabIndexSelected = new BehaviorSubject<number>(0);
  public currentPerson = new BehaviorSubject<Person>(null as unknown as Person);
  public currentPersonEdited!: Person;
  public currentPersonNotEdited!: Person;
  public personEditedId!: string;
  public personEdited = new BehaviorSubject<boolean>(false);

  public editForm = this.formBuilder.group({
    title: ['', Validators.required],
    sex: ['', Validators.required],
    weight_max: ['', Validators.required],
    height_max: ['', Validators.required],
    dates_of_birth_used: ['', Validators.required],
    hair: ['', Validators.required],
    eyes: ['', Validators.required],
    nationality: ['', Validators.required],
    place_of_birth: ['', Validators.required]
  });

  public personsFieldOffices = new Set();

  public tableColumnNamesEdited: string[] = [];

  constructor(public matDialog: MatDialog, private formBuilder: FormBuilder, private apiService: ApiService) { }

  public getTableColumnNames(): (string | TableColumnNames)[] {
    if(!this.tableColumnNamesEdited.length) {
      return Object.values(TableColumnNames).filter(elem =>
        typeof elem === "string"
      );
    } else {
      return this.tableColumnNamesEdited;
    }
  }

  public getPersonsFieldOffices(persons: Person[]): void {
    persons.map((person: Person) => {
      if(person['field_offices']) {
        person['field_offices'].map((fieldOffice: string) => this.personsFieldOffices.add(fieldOffice));
      }
    });
  }

  public getPersons(page: number): Observable<Person[]> {
    if(this.requestWithFilters) {
      return forkJoin([
        this.apiService.getPersons(page, this.requestFilters.hairColor, this.requestFilters.eyesColor, 
          this.requestFilters.weight, this.requestFilters.height),
        this.getPersonsEdited()
      ]).pipe(
        map(([persons, editedPersons]) => {
          if(page === 1) {
            this.totalNumberOfPersons = persons.total;
            this.numberOfPersonsOnPage = persons.items.length;
          }
          if(!localStorage.getItem('field_offices')) {
            this.getPersonsFieldOffices(persons.items);
          }
          return this.setPersonsEditedStatus(persons.items, editedPersons)
        })
      );
    } else {
      return forkJoin([
        this.apiService.getPersons(page, "", "", "", ""),
        this.getPersonsEdited()
      ]).pipe(
        map(([persons, editedPersons]) => {
          if(page === 1) {
            this.totalNumberOfPersons = persons.total;
            this.numberOfPersonsOnPage = persons.items.length;
          }
          if(!localStorage.getItem('field_offices')) {
            this.getPersonsFieldOffices(persons.items);
          }
          return this.setPersonsEditedStatus(persons.items, editedPersons)
        })
      );
    }
  }

  public setPersonsEditedStatus(persons: Person[], personsEdited: Person[]): Person[] {
    let personsEditedId = new Set();
    personsEdited.forEach(personEdited => personsEditedId.add(personEdited["@id"]));
    persons.map(person => {
      if(personsEditedId.has(person["@id"])) {
        person.edited = true;
      }
    });
    return persons;
  }

  public editCard(person: Person): void {
    this.getPersonEditedId(person);
    const editCardWindow = new MatDialogConfig();
    editCardWindow.width = "100%";
    editCardWindow.height = "100%";
    this.matDialog.open(FbiWantedEditFormComponent, editCardWindow).updatePosition({ left: "11%" });
  }

  public getPersonEditedId(person: Person): void {
    this.getPersonsEdited().pipe(untilDestroyed(this)).subscribe(persons => {
      const personsEdited = persons.filter(personEdited => person["@id"] === personEdited["@id"]);
      if(personsEdited.length) {
        this.personEditedId = personsEdited[0].id;
      } else {
        this.personEditedId = '';
      }
    });
  }

  public getPersonsEdited(): Observable<Person[]> {
    return this.apiService.getPersonsEdited();
  }

  public addPersonsEdited(person: Person): Observable<Person> {
    return this.apiService.addEditedPerson(person, this.personEditedId);
  }
}
