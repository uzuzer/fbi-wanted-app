import { TestBed } from '@angular/core/testing';

import { FbiWantedInterceptor } from './fbi-wanted.interceptor';

describe('FbiWantedInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      FbiWantedInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: FbiWantedInterceptor = TestBed.inject(FbiWantedInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
