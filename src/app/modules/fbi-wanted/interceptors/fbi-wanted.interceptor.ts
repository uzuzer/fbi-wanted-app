import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class FbiWantedInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let parameter = localStorage.getItem('field_offices');
    
    if (parameter && request.url.includes('https://api.fbi.gov/wanted/v1/list')) {
      const parameterRequest = request.clone({
          params: request.params.set('field_offices', parameter)
      });
      return next.handle(parameterRequest);
    } else {
      return next.handle(request);
    }
  }
}
