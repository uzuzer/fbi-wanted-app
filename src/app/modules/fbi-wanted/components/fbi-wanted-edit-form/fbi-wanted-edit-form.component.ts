import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup } from '@angular/forms';

import { FbiWantedService } from '../../services/fbi-wanted.service';
import { Person } from '../../../../shared/interfaces/person';

@UntilDestroy()
@Component({
  selector: 'fw-fbi-wanted-edit-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './fbi-wanted-edit-form.component.html',
  styleUrls: ['./fbi-wanted-edit-form.component.scss']
})
export class FbiWantedEditFormComponent implements OnInit {
  private editablePerson!: Person;
  public editForm!: FormGroup;

  constructor(public dialogRef: MatDialogRef<FbiWantedEditFormComponent>, private fbiWantedService: FbiWantedService) { }

  ngOnInit(): void { 
    this.buildForm();
    this.getCurrentPerson();
  }

  private buildForm(): void {
    this.editForm = this.fbiWantedService.editForm;
  }

  private getCurrentPerson(): void {
    this.fbiWantedService.currentPerson.pipe(untilDestroyed(this)).subscribe(person => {
      this.editablePerson = person;
      this.editForm.patchValue(person);
    });
  }

  public onEditPerson(): void {
    this.editablePerson.edited = true;
    this.fbiWantedService.addPersonsEdited({...this.editablePerson, ...this.editForm.getRawValue()}).pipe(untilDestroyed(this)).subscribe(_ => {
      this.dialogRef.close();
      this.editForm.removeControl('newData');
      this.fbiWantedService.personEdited.next(true);
    });
  }

  public onCloseForm(): void {
    this.editForm.removeControl('newData');
    this.dialogRef.close();
  }
}
