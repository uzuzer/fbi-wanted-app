import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FbiWantedEditFormComponent } from './fbi-wanted-edit-form.component';

describe('FbiWantedEditFormComponent', () => {
  let component: FbiWantedEditFormComponent;
  let fixture: ComponentFixture<FbiWantedEditFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FbiWantedEditFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FbiWantedEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
