import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FbiWantedCardDetailsComponent } from './fbi-wanted-card-details.component';

describe('FbiWantedCardDetailsComponent', () => {
  let component: FbiWantedCardDetailsComponent;
  let fixture: ComponentFixture<FbiWantedCardDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FbiWantedCardDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FbiWantedCardDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
