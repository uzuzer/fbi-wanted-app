import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

import { Person } from '../../../../shared/interfaces/person';
import { FbiWantedService } from '../../services/fbi-wanted.service';

@UntilDestroy()
@Component({
  selector: 'fw-fbi-wanted-card-details',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './fbi-wanted-card-details.component.html',
  styleUrls: ['./fbi-wanted-card-details.component.scss']
})
export class FbiWantedCardDetailsComponent implements OnInit {
  public personSelected!: Person;

  constructor(private ref: ChangeDetectorRef, private fbiWantedService: FbiWantedService) { }

  public ngOnInit(): void { 
    this.fbiWantedService.tabIndexSelected.pipe(untilDestroyed(this)).subscribe(index => {
      if(!index) {
        this.personSelected = this.fbiWantedService.currentPersonNotEdited
      } else {
        this.personSelected = this.fbiWantedService.currentPersonEdited
      }
      this.ref.markForCheck();
    });
  }
}
