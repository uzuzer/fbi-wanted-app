import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FbiWantedCardListComponent } from './fbi-wanted-card-list.component';

describe('FbiWantedCardListComponent', () => {
  let component: FbiWantedCardListComponent;
  let fixture: ComponentFixture<FbiWantedCardListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FbiWantedCardListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FbiWantedCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
