import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { NgForm } from '@angular/forms';

import { Person } from '../../../../shared/interfaces/person';
import { FbiWantedService } from '../../services/fbi-wanted.service';

@UntilDestroy()
@Component({
  selector: 'fw-fbi-wanted-card-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './fbi-wanted-card-list.component.html',
  styleUrls: ['./fbi-wanted-card-list.component.scss']
})
export class FbiWantedCardListComponent implements OnInit {
  public currentPage: number = 1;

  public persons!: Person[];
  public personClicked!: Person;

  constructor(private ref: ChangeDetectorRef, public fbiWantedService: FbiWantedService) { }

  public ngOnInit(): void {
    this.getPersons(this.fbiWantedService.minPage);
    this.fbiWantedService.personEdited.pipe(untilDestroyed(this)).subscribe(_ => {
      this.ref.markForCheck();
    });
  }

  public onNextPage(): void {
    if(this.currentPage < Math.ceil(this.fbiWantedService.totalNumberOfPersons / this.fbiWantedService.numberOfPersonsOnPage)) {
      this.currentPage++;
    } else {
      this.currentPage = this.fbiWantedService.minPage;
    }
    this.getPersons(this.currentPage);
  }

  public onPreviousPage(): void {
    if(this.currentPage > this.fbiWantedService.minPage) {
      this.currentPage--;
    } else {
      this.currentPage = Math.ceil(this.fbiWantedService.totalNumberOfPersons / this.fbiWantedService.numberOfPersonsOnPage);
    }
    this.getPersons(this.currentPage);
  }

  public getPersons(page: number): void {
    this.fbiWantedService.getPersons(page).pipe(untilDestroyed(this)).subscribe(persons => {
      this.persons = persons;
      this.onSelectedCard(this.persons[0]);
      this.ref.markForCheck();
    });
  }

  public onSelectedCard(person: Person): void {
    this.personClicked = person;
    this.fbiWantedService.currentPersonNotEdited = person;
    this.fbiWantedService.currentPerson.next(this.fbiWantedService.currentPersonNotEdited);
    this.fbiWantedService.tabIndexSelected.next(0);
  }

  public onSearchWithFilters(withFilters: boolean, form: NgForm): void {
    if(withFilters) {
      this.fbiWantedService.requestWithFilters = withFilters;
      this.fbiWantedService.requestFilters = form.value;
    } else {
      this.fbiWantedService.requestWithFilters = withFilters;
    }
    this.currentPage = this.fbiWantedService.minPage;
    form.resetForm();
    this.getPersons(this.currentPage);
  }

  public onEditCard(person: Person): void {
    this.fbiWantedService.editCard(person);
  }

  public onEditedCard(): void {
    this.fbiWantedService.personEdited.next(true);
    this.fbiWantedService.getPersonsEdited().pipe(untilDestroyed(this)).subscribe(personsEdited => {
      this.fbiWantedService.tabIndexSelected.next(1);
      this.fbiWantedService.currentPersonEdited = personsEdited.filter(person => this.personClicked["@id"] === person["@id"])[0];
      this.fbiWantedService.currentPerson.next(this.fbiWantedService.currentPersonEdited);
      this.ref.markForCheck();
    });
  }
}
