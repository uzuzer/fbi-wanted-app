import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FbiWantedSexPickerComponent } from './fbi-wanted-sex-picker.component';

describe('FbiWantedSexPickerComponent', () => {
  let component: FbiWantedSexPickerComponent;
  let fixture: ComponentFixture<FbiWantedSexPickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FbiWantedSexPickerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FbiWantedSexPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
