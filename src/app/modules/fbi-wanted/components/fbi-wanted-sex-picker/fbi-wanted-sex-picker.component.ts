import { Component, OnInit, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'fw-fbi-wanted-sex-picker',
  templateUrl: './fbi-wanted-sex-picker.component.html',
  styleUrls: ['./fbi-wanted-sex-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FbiWantedSexPickerComponent),
      multi: true
    }
  ]
})
export class FbiWantedSexPickerComponent implements OnInit {
  public personSex = [
    { src: "https://cdn-icons-png.flaticon.com/512/97/97373.png", value: "Female" }, 
    { src: "https://cdn-icons-png.flaticon.com/512/97/97394.png", value: "Male"}
  ];
  public selectedPersonSex: string = "";

  private onChange = (value: any) => {};
  private onTouch = (value: any) => {};
  private disabled!: boolean;

  constructor() { }

  public ngOnInit(): void { }

  public sexClicked(sex: string): void {
    this.selectedPersonSex = sex;
    this.onChange(this.selectedPersonSex);
    this.onTouch(true);
  }

  public writeValue(value: string): void {
    this.selectedPersonSex = value;
  }

  public registerOnChange(func: any): void {
    this.onChange = func;
  }

  public registerOnTouched(func: any): void {
    this.onTouch = func;
  }

  public get isDisabled(): boolean {
    return this.disabled;
  }
  
  public setIsDisabled(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
