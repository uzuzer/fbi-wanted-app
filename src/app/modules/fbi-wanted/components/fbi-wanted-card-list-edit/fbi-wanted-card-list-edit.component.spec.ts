import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FbiWantedCardListEditComponent } from './fbi-wanted-card-list-edit.component';

describe('FbiWantedCardListEditComponent', () => {
  let component: FbiWantedCardListEditComponent;
  let fixture: ComponentFixture<FbiWantedCardListEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FbiWantedCardListEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FbiWantedCardListEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
