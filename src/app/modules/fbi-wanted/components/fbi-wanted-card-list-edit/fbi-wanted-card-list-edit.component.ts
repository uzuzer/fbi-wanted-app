import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { filter } from 'rxjs/operators';

import { Person } from '../../../../shared/interfaces/person';
import { FbiWantedService } from '../../services/fbi-wanted.service';

@UntilDestroy()
@Component({
  selector: 'fw-fbi-wanted-card-list-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './fbi-wanted-card-list-edit.component.html',
  styleUrls: ['./fbi-wanted-card-list-edit.component.scss']
})
export class FbiWantedCardListEditComponent implements OnInit {
  public persons!: Person[];
  public personClicked!: Person | undefined;

  constructor(private ref: ChangeDetectorRef, public fbiWantedService: FbiWantedService) { }

  public ngOnInit(): void { 
    this.fbiWantedService.personEdited.pipe(untilDestroyed(this)).subscribe(_ => {
      this.getPersons();
    });

    this.fbiWantedService.currentPerson.pipe(untilDestroyed(this), filter(Boolean)).subscribe(currentPerson => {
      if(this.persons) {
        this.personClicked = this.persons.find(person => person["@id"] === (currentPerson as Person)["@id"]);
        this.ref.markForCheck();
      }
    });
  }

  public getPersons(): void {
    this.fbiWantedService.getPersonsEdited().pipe(untilDestroyed(this)).subscribe(persons => {
      this.persons = persons;
      this.ref.markForCheck();
    });
  }

  public onSelectedCard(person: Person): void {
    this.personClicked = person;
    this.fbiWantedService.currentPersonEdited = person;
    this.fbiWantedService.currentPerson.next(this.fbiWantedService.currentPersonEdited);
    this.fbiWantedService.tabIndexSelected.next(1);
  }

  public onEditCard(person: Person): void {
    this.fbiWantedService.editCard(person);
  }
}
