import { Component, OnInit, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, Validators, FormBuilder, 
  FormGroup, FormArray } from '@angular/forms';
import { faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';

import { InputDataTypes } from '../../data/input-data-types';
import { FormValidator } from '../../validators/form-validator';
import { FbiWantedService } from '../../services/fbi-wanted.service';

@Component({
  selector: 'fw-fbi-wanted-edit-form-new-input',
  templateUrl: './fbi-wanted-edit-form-new-input.component.html',
  styleUrls: ['./fbi-wanted-edit-form-new-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FbiWantedEditFormNewInputComponent),
      multi: true
    }
  ]
})
export class FbiWantedEditFormNewInputComponent implements OnInit {
  public plusIcon = faPlus;
  public deleteIcon = faTrash;

  private onChange = (value: any) => {};
  private onTouch = (value: any) => {};

  public editForm!: FormGroup;
  private selectedInputName!: string;

  private formValidator = new FormValidator();

  constructor(private formBuilder: FormBuilder, public fbiWantedService: FbiWantedService) { }

  ngOnInit() {
    this.fbiWantedService.editForm.addControl('newData', this.formBuilder.array([], this.formValidator.uniqueInputsNamesValidator()));
    this.editForm = this.fbiWantedService.editForm;
  }

  public writeValue(value: string): void {
    this.selectedInputName = value;
  }

  public registerOnChange(func: any): void {
    this.onChange = func;
  }

  public registerOnTouched(func: any): void {
    this.onTouch = func;
  }

  public get newData(): FormArray {
    return this.editForm.get('newData') as FormArray;
  }

  public getInputDataTypes(): (string | InputDataTypes)[] {
    return Object.values(InputDataTypes).filter(elem =>
      typeof elem === "string"
    );
  }

  public addInput(propertyName: string, propertyDataType: string): void {
    this.selectedInputName = propertyName;
    this.onChange(this.selectedInputName);
    this.onTouch(true);
    this.formValidator.selectedInputDataType = propertyDataType;
    this.newData.push(this.formBuilder.group({ [propertyName]: ['', [Validators.required, this.formValidator.newInputsValidator]] }));
  }

  public deleteInput(index: number): void {
    this.newData.removeAt(index);
  }
}
