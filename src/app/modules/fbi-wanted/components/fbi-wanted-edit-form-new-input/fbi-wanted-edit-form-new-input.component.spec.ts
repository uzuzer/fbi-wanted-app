import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FbiWantedEditFormNewInputComponent } from './fbi-wanted-edit-form-new-input.component';

describe('FbiWantedEditFormNewInputComponent', () => {
  let component: FbiWantedEditFormNewInputComponent;
  let fixture: ComponentFixture<FbiWantedEditFormNewInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FbiWantedEditFormNewInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FbiWantedEditFormNewInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
