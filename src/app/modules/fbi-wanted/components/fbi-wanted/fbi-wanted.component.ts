import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Person } from 'src/app/shared/interfaces/person';

import { FbiWantedService } from '../../services/fbi-wanted.service';

@UntilDestroy()
@Component({
  selector: 'fw-fbi-wanted',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './fbi-wanted.component.html',
  styleUrls: ['./fbi-wanted.component.scss']
})
export class FbiWantedComponent implements OnInit {
  public selectedTabIndex!: number;

  constructor(public ref: ChangeDetectorRef, public fbiWantedService: FbiWantedService) { }

  public ngOnInit(): void { 
    this.fbiWantedService.tabIndexSelected.pipe(untilDestroyed(this)).subscribe(tabIndex => {
      this.selectedTabIndex = tabIndex;
      this.ref.markForCheck();
    });
  }

  public onTable(event: MatTabChangeEvent): void {
    const selectedTabIndex = event.index;
    if(selectedTabIndex === 0) {
      this.fbiWantedService.currentPerson.next(this.fbiWantedService.currentPersonNotEdited);
      this.fbiWantedService.tabIndexSelected.next(0);
    } else if(selectedTabIndex === 1) {
      this.fbiWantedService.currentPerson.next(this.fbiWantedService.currentPersonEdited);
      this.fbiWantedService.tabIndexSelected.next(1);
    }
  }
}
