import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FbiWantedComponent } from './fbi-wanted.component';

describe('FbiWantedComponent', () => {
  let component: FbiWantedComponent;
  let fixture: ComponentFixture<FbiWantedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FbiWantedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FbiWantedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
