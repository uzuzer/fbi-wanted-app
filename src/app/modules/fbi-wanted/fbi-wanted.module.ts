import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule } from '@angular/material/input';
import { HttpClientModule } from '@angular/common/http';

import { FbiWantedComponent } from './components/fbi-wanted/fbi-wanted.component';
import { FbiWantedCardListComponent } from './components/fbi-wanted-card-list/fbi-wanted-card-list.component';
import { FbiWantedCardDetailsComponent } from './components/fbi-wanted-card-details/fbi-wanted-card-details.component';
import { FbiWantedPipe } from './pipes/fbi-wanted.pipe';
import { FbiWantedEditFormComponent } from './components/fbi-wanted-edit-form/fbi-wanted-edit-form.component';
import { FbiWantedCardListEditComponent } from './components/fbi-wanted-card-list-edit/fbi-wanted-card-list-edit.component';
import { FbiWantedSexPickerComponent } from './components/fbi-wanted-sex-picker/fbi-wanted-sex-picker.component';
import { FbiWantedEditFormNewInputComponent } from './components/fbi-wanted-edit-form-new-input/fbi-wanted-edit-form-new-input.component';

@NgModule({
  declarations: [
    FbiWantedComponent,
    FbiWantedCardListComponent,
    FbiWantedCardDetailsComponent,
    FbiWantedPipe,
    FbiWantedEditFormComponent,
    FbiWantedCardListEditComponent,
    FbiWantedSexPickerComponent,
    FbiWantedEditFormNewInputComponent
  ],
  imports: [
    MatButtonToggleModule,
    MatTableModule,
    MatTabsModule,
    MatDialogModule,
    MatStepperModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    CommonModule,
    HttpClientModule
  ]
})
export class FbiWantedModule { }
