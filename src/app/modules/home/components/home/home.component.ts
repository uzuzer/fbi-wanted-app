import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { TreeNode } from '../../../../shared/interfaces/tree-node';

@Component({
  selector: 'fw-home',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor() { }

  public ngOnInit(): void { }

  public treeNodes: TreeNode[] = [
    {
      id: 'children 1', 
      children: [
      {
        id: 'children 1.1', 
        children: [
          {
            id: 'children 1.1.1', 
            children: []
          },
          {
            id: 'children 1.1.2', 
            children: []
          }
      ]}
      ,
      {
        id: 'children 1.2', 
        children: [
          {
            id: 'children 1.2.1', 
            children: []
          }
      ]}
    ]},
    {
      id: 'children 2', 
      children: []
    }
  ];

}
