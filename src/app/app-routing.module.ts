import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthorizationGuard } from './modules/authorization/guards/authorization.guard';

import { HomeComponent } from './modules/home/components/home/home.component';
import { FbiWantedComponent } from './modules/fbi-wanted/components/fbi-wanted/fbi-wanted.component';
import { SettingsComponent } from './modules/settings/components/settings/settings.component';

const routes: Routes = [
  { path: 'main', component: HomeComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthorizationGuard] },
  { path: 'fbi-wanted', component: FbiWantedComponent, canActivate: [AuthorizationGuard] },
  { path: 'settings', component: SettingsComponent, canActivate: [AuthorizationGuard] },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

