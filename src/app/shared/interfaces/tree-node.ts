export interface TreeNode {
  id: string;
  children: TreeNode[];
}