import { Image } from './image';

export interface Person {
  "@id": string;
  id: string;
  edited: boolean;
  title: string;
  sex: string;
  images: Image[];
  weight_max: number;
  height_max: number;
  hair: string;
  eyes: string;
  dates_of_birth_used: string;
  nationality: string;
  place_of_birth: string;
  newData: any[];
  field_offices: string[];
  race_raw: string;
}