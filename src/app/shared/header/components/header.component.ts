import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Router } from '@angular/router';

import { User } from '../../../shared/interfaces/user';
import { AuthorizationService } from '../../../modules/authorization/services/authorization.service';
import { AuthorizationComponent } from '../../../modules/authorization/components/authorization/authorization.component';

@UntilDestroy()
@Component({
  selector: 'fw-header',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public currentUser!: User;
  private loginClick: boolean = false;

  constructor(public authorizationService: AuthorizationService, public matDialog: MatDialog, 
    private ref: ChangeDetectorRef, private router: Router) { }

  ngOnInit(): void {
    this.authorizationService.currentUser.pipe(untilDestroyed(this)).subscribe(user => {
      if(user){
        this.currentUser = user;
		    this.ref.markForCheck();
      }
    });
   }

  public onLogin(): void {
    if(!this.authorizationService.currentUser.value.login && !this.loginClick) {
      this.loginClick = true;
      const authorizationWindow = new MatDialogConfig();
      authorizationWindow.width = "100%";
      authorizationWindow.height = "100%";
      this.matDialog.open(AuthorizationComponent, authorizationWindow).updatePosition({ left: "11.7%"});
    }
  }

  public onLogout(): void {
    this.loginClick = false;
    this.router.navigate(['/main']);
    this.authorizationService.signOut();
  }
}
