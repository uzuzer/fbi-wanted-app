import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';

import { AuthorizationModule } from '../../modules/authorization/authorization.module';
import { HeaderComponent } from './components/header.component';

@NgModule({
  declarations: [
    HeaderComponent
  ],
  imports: [
    AuthorizationModule,
    MatDialogModule,
    RouterModule,
    CommonModule
  ],
  exports: [
    HeaderComponent
  ]
})
export class HeaderModule { }
