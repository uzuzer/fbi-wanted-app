import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../../interfaces/user';
import { Person } from '../../interfaces/person';
import { PagedResponse } from '../../interfaces/paged-response';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {  
  constructor(private http: HttpClient) { }

  public getUser(login: string, password: string): Observable<User> {
    return this.http.get<User[]>(environment.usersUrl).pipe(map(users => {
      const usersFiltered = users.filter(user => user.login === login && user.password === password);
      return usersFiltered[0];
    }));
  }

  public getPersons(page: number, hairColor: string, eyesColor: string, weightMax: string, heightMax: string): Observable<PagedResponse<Person>> {
    let urlFilters = "";
    if(hairColor) {
      urlFilters += `&hair=${hairColor}`;
    } 
    if(eyesColor) {
      urlFilters += `&eyes=${eyesColor}`;
    }
    if(weightMax) {
      urlFilters += `&weight_max=${weightMax}`;
    } 
    if(heightMax) {
      urlFilters += `&height_max=${heightMax}`;
    }
    return this.http.get<PagedResponse<Person>>(environment.fbiWantedUrl + `?page=${page}` + urlFilters);
  }

  public getPersonsEdited(): Observable<Person[]> {
    return this.http.get<Person[]>(environment.fbiWantedEditedUrl);
  }

  public addEditedPerson(person: Person, id: string): Observable<Person> {
    if(id) {
      return this.http.put<Person>(environment.fbiWantedEditedUrl + '/' + id, person);
    } else {
      return this.http.post<Person>(environment.fbiWantedEditedUrl, person);
    }
  }
}
