import { Component, OnInit, ChangeDetectionStrategy,ChangeDetectorRef } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

import { User } from '../../../shared/interfaces/user';
import { AuthorizationService } from '../../../modules/authorization/services/authorization.service';

@UntilDestroy()
@Component({
  selector: 'fw-sidebar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public currentUser!: User;

  constructor(private authorizationService: AuthorizationService, private ref: ChangeDetectorRef) { }

  public ngOnInit(): void { 
    this.authorizationService.currentUser.pipe(untilDestroyed(this)).subscribe(user => {
      if(user){
        this.currentUser = user;
		this.ref.markForCheck();
      }
    });
  }
}
