import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar'; 
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';

import { SidebarComponent } from './components/sidebar.component';

@NgModule({
  declarations: [
    SidebarComponent
  ],
  imports: [
    RouterModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    CommonModule
  ],
  exports: [
    SidebarComponent
  ]
})
export class SidebarModule { }
