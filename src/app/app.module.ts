import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { MatListModule } from  '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTableModule } from '@angular/material/table';
import { AuthorizationModule } from './modules/authorization/authorization.module';
import { FbiWantedModule } from './modules/fbi-wanted/fbi-wanted.module';
import { HomeModule } from './modules/home/home.module';
import { SettingsModule } from './modules/settings/settings.module';
import { SharedModule } from './shared/shared.module';
import { AuthorizationGuard } from './modules/authorization/guards/authorization.guard';
import { FbiWantedInterceptor } from './modules/fbi-wanted/interceptors/fbi-wanted.interceptor';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    MatListModule,
    MatSidenavModule,
    MatTableModule,
    AuthorizationModule,
    FbiWantedModule,
    HomeModule,
    SettingsModule,
    SharedModule,
    CommonModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [
    AuthorizationGuard,
    { 
      provide: HTTP_INTERCEPTORS, 
      useClass: FbiWantedInterceptor, 
      multi: true 
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
